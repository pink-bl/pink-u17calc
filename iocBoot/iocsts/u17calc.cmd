#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

##test
#epicsEnvSet("IOCPREFIX", "PINK:U17DCM:")

## macros
epicsEnvSet("PREFIX", "$(IOCPREFIX)")

## Load record instances
dbLoadRecords("${TOP}/iocBoot/${IOC}/u17.db", "P=$(PREFIX)")

cd "${TOP}/iocBoot/${IOC}"

# autosave settings
set_requestfile_path("${TOP}/iocBoot/${IOC}")
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

# autosave
create_monitor_set("auto_settings.req", 30, "P=$(PREFIX)")

## Start any sequence programs
#seq sncxxx,"user=epics"
