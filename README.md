# pink-u17calc
IOC to calculate DCM/U17 relationship corrections

## Calculations
### variables
**E** : Energy\
**G** : Nominal Gap\
**M** : Better Gap\
**A** : Slope\
**B** : Offset\
**N** : New Energy

### equations
```math
A1 = (E2-E1)/(G2-G1)\\
B1 = E1-(A1*G1)\\
A2 = (E2-E1)/(M2-M1)\\
B2 = E1-(A2*M1)\\
N1 = (A2*G1)+B2\\
N2 = (A2*G2)+B2\\

IdSlope = A1/A2\\
IdOffset = (A1*G1)+B1-N1
```
